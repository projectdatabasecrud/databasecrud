/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.werapan.databaseproject.model;

import com.werapan.databaseproject.dao.ProductDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author admin
 */
public class OrderDetail {
    private int id;
    Product product;
    private String productName;
    private double productPrice;
    private int qty;
    private Orders order;

    public OrderDetail(int id, Product product, String productName, double productPrice, int qty, Orders order) {
        this.id = id;
        this.product = product;
        this.productName = productName;
        this.productPrice = productPrice;
        this.qty = qty;
        this.order = order;
    }

    public OrderDetail(Product product, String productName, double productPrice, int qty, Orders order) {
        this.product = product;
        this.productName = productName;
        this.productPrice = productPrice;
        this.qty = qty;
        this.order = order;
    }

    public OrderDetail() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public Orders getOrder() {
        return order;
    }

    public void setOrder(Orders order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return "OrderDetail{" + "id=" + id + ", product=" + product + ", productName=" + productName + ", productPrice=" + productPrice + ", qty=" + qty + "}";
    }
    
    public double getTotal() {
        return qty * productPrice;
    }
    
    public static OrderDetail fromRS(ResultSet rs) {
        ProductDao productDao = new ProductDao();
        OrderDetail orderDetail = new OrderDetail();
        try {
            orderDetail.setId(rs.getInt("order_detail_id"));
            int productId = rs.getInt("product_id");
            Product item = productDao.get(productId);
            orderDetail.setProduct(item);
            orderDetail.setQty(rs.getInt("qty"));
            orderDetail.setProductName(rs.getString("product_name"));
            orderDetail.setProductPrice(rs.getDouble("product_price"));
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } 
        return orderDetail;
    }
}
